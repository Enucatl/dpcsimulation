#!/usr/bin/env python
import numpy as np
from scipy import signal

from dpcsimulation.constants import hbar_c


class IncommensurableLengths(Exception):
    """The grating doesn't fit with an integer number of periods"""
    pass


class Grating(np.ndarray):
    """A grating object has a:
        material (symbol)

        pitch is the sum of the width of one 'black' (blocking) and one 'white'
        (transmitting) bar of the grating, that is the length of one period
        (cm)

        duty_cycle is the ratio of the width of the 'black' bar to the
        period

        number_of_periods is the total number of periods in the grating

        thickness (cm)
    """
    def __new__(cls, shape, *args, physical_size, material,
                pitch, duty_cycle, **kwargs):
        obj = np.ndarray.__new__(cls, shape, dtype=float, *args, **kwargs)
        obj.material = material
        obj.pitch = pitch
        obj.duty_cycle = duty_cycle
        obj.physical_size = physical_size
        number_of_cells = obj.shape[0]
        number_of_periods = int(obj.physical_size / obj.pitch)
        if number_of_cells % number_of_periods:
            raise IncommensurableLengths("""The period of the grating does
            not fit an integer number of times in the wave length""")
        t = np.linspace(0, 2 * np.pi * number_of_periods, number_of_cells,
                        endpoint=False)
        obj[...] = 0.5 * (
            1 + signal.square(t, duty=obj.duty_cycle))
        return obj

    def __array_finalize__(self, obj):
        if obj is None:
            return


def calculate_thickness(energy, delta, desired_phase_shift):
    """
    energy (keV)

    desired_phase_shift (rad)
    """
    k = energy / hbar_c
    return desired_phase_shift / (k * delta)

if __name__ == '__main__':
    from dpcsimulation.wave import PolychromaticWave
    wave = PolychromaticWave(shape=(10, 190))
    g = Grating(shape=20, physical_size=5.6, material="Au",
                pitch=2.8, duty_cycle=0.5, thickness=0.08)
    print(g)
