#!/usr/bin/env python

from dpcsimulation.constants import hbar_c
import numpy as np

from nist_lookup.nist_lookup import get_graph_delta
from nist_lookup.nist_lookup import get_graph_beta


class WrongEnergies(Exception):
    pass


class PolychromaticWave(np.ndarray):
    """
    complex wave (energy in keV) array of length n
    physical length in cm

    frequencies needed for propagation

    """

    def __new__(cls, shape, *args, energies=np.arange(10, 200),
                physical_size=1, **kwargs):
        obj = np.ndarray.__new__(cls, shape, dtype=complex, *args, **kwargs)
        # save energy and wave number
        obj.energies = energies
        obj.ks = energies / hbar_c
        obj.physical_size = physical_size
        number_of_cells = obj.shape[0]
        cell_size = obj.physical_size / number_of_cells
        cell_numbers = np.arange(number_of_cells) - number_of_cells // 2
        obj.frequencies = np.fft.ifftshift(cell_numbers) * (
            2 * np.pi / (number_of_cells * cell_size)
        )
        obj[...] = 1
        if shape[1] != energies.shape[0]:
            raise WrongEnergies("{0} != {1}".format(
                    shape[1], energies.shape[0]))
        return obj

    def __array_finalize__(self, obj):
        if obj is None:
            return
        else:
            self.energies = getattr(obj, "energies", None)
            self.ks = getattr(obj, "ks", None)
            self.physical_size = getattr(obj, "physical_size", None)
            self.frequencies = getattr(obj, "frequencies", None)

    def __array_wrap__(self, out_arr, context=None):
        return np.ndarray.__array_wrap__(self, out_arr, context)


def wave_fft(wave, n=None, axis=-1):
    """Copy attributes after np.fft.fft"""
    result = np.fft.fft(wave, n=n, axis=axis)
    result = result.view(PolychromaticWave)
    result.__array_finalize__(wave)
    return result


def wave_ifft(wave, n=None, axis=-1):
    """Copy attributes after np.fft.ifft"""
    result = np.fft.ifft(wave, n=n, axis=axis)
    result = result.view(PolychromaticWave)
    result.__array_finalize__(wave)
    return result


def propagate_freely(wave, distance):
    """propagate_freely the wave with the fourier propagator for each energy.
    distance in cm

    """
    fourier_space_propagator = np.exp(-1j *
                                      np.outer(np.square(wave.frequencies),
                                               distance / (2 * wave.ks)))
    fourier_space_wave = wave_fft(wave, axis=0)
    fourier_space_wave = np.multiply(
        fourier_space_wave, fourier_space_propagator)
    real_space_wave = wave_ifft(fourier_space_wave, axis=0)
    result = np.multiply(real_space_wave, np.exp(1j * wave.ks * distance))
    return result


def propagate_through_material(wave, thickness, table_raw):
    """Thickness (cm)
    raw table for material from nist_lookup.nist_lookup.get_formatted_table"""
    graph_delta = get_graph_delta(table_raw)
    table_raw.seek(0)
    graph_beta = get_graph_beta(table_raw)
    table_raw.seek(0)
    deltas = graph_delta(wave.energies)
    betas = graph_beta(wave.energies)
    exponent = np.outer(thickness, -1j * wave.ks * (deltas - 1j * betas))
    wave = np.multiply(wave, np.exp(exponent))
    return wave


def intensity(wave, weights=None):
    """Incoherent sum of the amplitudes for different energies."""
    if weights is None:
        weights = np.ones(shape=wave.shape[1])
    return np.dot(np.square(np.absolute(wave)), weights.T)

if __name__ == '__main__':
    w = PolychromaticWave(shape=(20, 190))
    print("ks", w.ks)
    print("energies", w.energies)
    print("spatial frequencies", w.frequencies)

    print("before propagation", w)
    w = propagate_freely(w, 1)
    print("after propagation", w)
    print(w.shape)

    print("ks", w.ks)
    print("energies", w.energies)
    from dpcsimulation.grating import Grating
    g = Grating(shape=20, physical_size=5.6, material="Au",
                pitch=2.8, duty_cycle=0.5)
    thickness = 0.08
    g *= thickness
    w = PolychromaticWave(shape=(20, 190))
    from nist_lookup.nist_lookup import get_formatted_table
    from io import StringIO
    raw_table = StringIO(get_formatted_table(g.material))
    w = propagate_through_material(w, g, raw_table)
    print(w)
    print(w.shape)
    print(type(w))
    i = intensity(w)
    print(i)
    print(i.shape)
