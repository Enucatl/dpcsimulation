from math import pi

"hbar c (keV cm) from http://physics.nist.gov/cgi-bin/cuu/Value?hbcmevf"
hbar_c = 197.3269718e-10

hc = hbar_c * 2 * pi
