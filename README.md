# Simulate a Talbot interferometer

Full wave simulation, including polychromatic sources.

## Requirements

- [progress_bar](https://bitbucket.org/Enucatl/progress_bar), a simple progress bar
- [spekcalc2numpy](https://bitbucket.org/Enucatl/spekcalc2numpy), read the
spectra simulated with
[SpekCalc](http://www.ncbi.nlm.nih.gov/pubmed/19724100)
- [nist_lookup](https://bitbucket.org/psitomcat/nist_lookup), download the
delta and beta tables from the NIST database
